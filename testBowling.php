<?php

require_once "bowling.php";

class TestBowling extends PHPUnit_Framework_TestCase {

    protected $game;
    protected function setUp(){
        $this->game=new Bowling();
    }

    public function pinsGenerator($numPins, $valorPins){
        for($i=0; $i<$numPins; $i++){
            $this->game->add($valorPins);
        }
    }

    public function testOne(){
        $this->game->add(5);
        $this->pinsGenerator(23,0);
        $this->assertEquals(5, $this->game->score());
    }

    public function testTwo(){
        $this->pinsGenerator(24,0);
        $this->assertEquals(0, $this->game->score());
    }

    public function testThreeOneSpare(){
        $this->game->add(5);
        $this->game->add(5);
        $this->game->add(1);
        $this->pinsGenerator(21,0);
        $this->assertEquals(12, $this->game->score());
    }

    public function testThreeOneSpareDiferentPins(){
        $this->game->add(3);
        $this->game->add(7);
        $this->game->add(1);
        $this->pinsGenerator(21,0);
        $this->assertEquals(12, $this->game->score());
    }

    public function testFourOneStrike(){
        $this->game->add(10);
        $this->game->add(1);
        $this->pinsGenerator(21,0);
        $this->assertEquals(12, $this->game->score());
    }

    public function testFourOneStrikeDiferentsPins(){
        $this->game->add(10);
        $this->game->add(3);
        $this->game->add(5);
        $this->pinsGenerator(21,0);
        $this->assertEquals(26, $this->game->score());
    }

    public function testFiveOneStrikeOneSpare(){
        $this->game->add(10);
        $this->game->add(1);
        $this->game->add(3);
        $this->game->add(5);
        $this->game->add(5);
        $this->game->add(3);
        $this->pinsGenerator(17, 0);
        $this->assertEquals(34, $this->game->score());
    }

    public function testSixAll9and0(){
        $this->pinsGenerator(10,9);
        $this->pinsGenerator(14,0);
        $this->assertEquals(90, $this->game->score());
    }

    public function testSevenTenParsOf5AndFinal5(){
        $this->pinsGenerator(20, 5);
        $this->game->add(5);
        $this->game->add(0);
        $this->game->add(0);
        $this->game->add(0);
        $this->assertEquals(150, $this->game->score());
    }

    public function testEightAllStrikes(){
        $this->game->add(10);
        $this->game->add(10);
        $this->game->add(10);
        $this->game->add(10);
        $this->game->add(10);
        $this->game->add(10);
        $this->game->add(10);
        $this->game->add(10);
        $this->game->add(10);
        $this->game->add(10);
        $this->game->add(10);
        $this->game->add(10);
        $this->assertEquals(300, $this->game->score());
    }

}