<?php

interface bowlingInterface{
    public function add($pins);
    public function score();

}

class Bowling implements bowlingInterface{
    private $pins=array();
    private $bonus=array(0,0,0);
    private $pinsOfTwoBalls=0;
    private $numberOfBalls=0;
    private $isStrike=false;


    public function add($pins){
        if($pins==10){
            $this->pins[]=$pins;
            $this->pins[]=0;
        }else{
            $this->pins[]=$pins;
        }
    }

    public function pinsAndBallsToZero(){
        $this->numberOfBalls=0;
        $this->pinsOfTwoBalls=0;
    }

    public function moveBonusarrayPosition(){
        $this->bonus[0]=$this->bonus[1];
        $this->bonus[1]=$this->bonus[2];
        $this->bonus[2]=0;
    }

    public function bonusValorOfPosition($position1, $position2){
        $this->bonus[1]+=$position1;
        $this->bonus[2]+=$position2;
    }

    public function addBonus($frames){
        $bonus=0;
        //Si no se hace esta comprobacion el ultimo Bonus
        // no se suma por que el estrike serepresenta por 10|0 hay que sumarselo a parte.
        /*if($frames>21 && $this->isStrike==true){
            $bonus=10;
        }*/

        if($this->isStrike==false){
            $this->moveBonusarrayPosition();
            for($i=0; $i<$this->bonus[0]; $i++){
                $bonus+=$this->pins[$frames];
            }
        }
        return $bonus;
    }

    public function operationForTwoBalls($frame){
        if($this->numberOfBalls==2 && $frame<20){
            $score=$this->pinsOfTwoBalls;
            $this->pinsAndBallsToZero();
            return $score;
        }else{
            return 0;
        }
    }

    public function operationForSpare(){
        if($this->pinsOfTwoBalls==10 && $this->numberOfBalls==2) {
            $this->bonusValorOfPosition(1,0);
            $this->pinsAndBallsToZero();
            return 10;
        }else{
            return 0;
        }
    }

    public function operationForStrike($frame){
        if($this->pinsOfTwoBalls==10 && $this->numberOfBalls==1) {
            if($frame >= 20) {
                $this->pinsOfTwoBalls=0;
                $this->isStrike=true;
                return 0;
            }
            $this->bonusValorOfPosition(1,1);
            $this->pinsOfTwoBalls=0;
            $this->isStrike=true;
            return 10;

        }else{
            $this->isStrike=false;
            return 0;
        }
    }

    public function score(){
        $score=0;
        $gameLength = count($this->pins);

        for($frames=0; $frames<$gameLength; $frames++) // TODO 24!!
        {
            $this->numberOfBalls++;
            $this->pinsOfTwoBalls+=$this->pins[$frames];

            $score+=$this->addBonus($frames);
            $score+=$this->operationForStrike($frames);
            $score+=$this->operationForSpare();
            $score+=$this->operationForTwoBalls($frames);

        }
    return $score;
    }
}



